import crypto from 'crypto';
import Transaction from "./../transaction/transaction";
import {calculateHash} from "./../utils/helpers";

export default class Block {
    timestamp: number;
    previousHash: string;
    transactions: Array<Transaction>;
    hash: string;
    nonce: number;

    constructor(timestamp: number, previousHash: string, transactions: Array<Transaction>) {
        this.timestamp = timestamp;
        this.previousHash = previousHash; 
        this.transactions = transactions;
        this.nonce = 0;
        this.hash = calculateHash(this.previousHash, this.timestamp, this.transactions, this.nonce);
    }

    /* */
    mineBlock(difficulty: number = 1): void {
        while (this.hash.substring(0, difficulty) !== Array(difficulty + 1).join('0')) {
            this.nonce++;
            this.hash = calculateHash(this.previousHash, this.timestamp, this.transactions, this.nonce);
        }
        console.log((`Block mined: ${this.hash}`));
    }

    hasValidTransactions(): boolean {
        for (const tx of this.transactions) {
            console.log(tx, tx.isValid());
            if (!tx.isValid()) {
                return false;
            }
        }
        return true;
    }

    method1() {
        /////dsadasdsa
    }
}