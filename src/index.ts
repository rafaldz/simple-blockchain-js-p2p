import Transaction from "./transaction/transaction";
import Blockchain from "./blockchain/blockchain";
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

const myKey = ec.keyFromPrivate('7c4c45907dec40c91bab3480c39032e90049f1a44f3e18c3e07c23e3273995cf');

const myWalletAddress = myKey.getPublic('hex');

console.log(myWalletAddress);

const exampleCoin = new Blockchain();

exampleCoin.minePendingTransactions(myWalletAddress);

const tx1 = new Transaction('exampleReceiverAddress', 100, myWalletAddress);
tx1.signTransaction(myKey);
exampleCoin.addTransaction(tx1);

exampleCoin.minePendingTransactions(myWalletAddress);

const tx2 = new Transaction('exampleReceiverAddress2', 50, myWalletAddress);
tx2.signTransaction(myKey);
exampleCoin.addTransaction(tx2);

exampleCoin.minePendingTransactions(myWalletAddress);

console.log();
console.log();
console.log(`Balance of the address ${myWalletAddress} is ${exampleCoin.getBalanceOfAddress(myWalletAddress)}`);

//exampleCoin.chain[1].transactions[0].amount = 10;

console.log();
console.log('Blockchain valid?', exampleCoin.isChainValid() ? 'Yes' : 'No');