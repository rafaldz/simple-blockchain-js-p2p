import { isAddressValid, calculateHash } from "./../utils/helpers";
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

export default class Transaction {
    fromAddress: string | null;
    toAddress: string;
    amount: number;
    signature: string;
    timestamp: number;

    constructor(toAddress: string, amount: number, fromAddress: string | null) {
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.amount = amount;
        this.timestamp = Date.now();
    }

    signTransaction(signingKey: any) {
        if (!isAddressValid(signingKey, this.fromAddress)) {
            throw new Error('You cannot sign a transaction for other wallet');
        }

        const hashTx = calculateHash('', Date.now(), []);

        const sig = signingKey.sign(hashTx, 'base64');

        this.signature = sig.toDER('hex');

        console.log(1);
    }

    //TODO verify, for now it's always returning false

    isValid() {
        //mining reward
        if (this.fromAddress === null) return true;

        if (!this.signature || this.signature.length === 0) {
            throw new Error("There's no signature in the trancastion" + `from address: ${this.fromAddress} to address: ${this.toAddress}`);
        }

        const publicKey = ec.keyFromPublic(this.fromAddress, 'hex');
        return publicKey.verify(calculateHash('', Date.now(), []), this.signature);
    }
}