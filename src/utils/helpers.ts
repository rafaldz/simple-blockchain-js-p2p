import crypto from 'crypto';
import Transaction from './../transaction/transaction';

export function calculateHash(previousHash: string, timestamp: number, transactions: Array<Transaction>, nonce = 0): string {
    return crypto.createHash('sha256').update(previousHash + timestamp + JSON.stringify(transactions) + nonce).digest('hex');
}

export function isAddressValid(signingKey: any, fromAddress: string | null): boolean {
    if (signingKey.getPublic('hex') !== fromAddress) {
        return false;
    } else {
        return true;
    }
}