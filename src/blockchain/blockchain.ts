import { calculateHash } from "./../utils/helpers";
import Block from "./../block/block"
import Transaction from "./../transaction/transaction";

export default class Blockchain {
    chain: Array<Block>;
    difficulty: number;
    miningReward: number;
     pendingTransactions: Array<Transaction>;

    constructor() {
        /* Create a genesis block to be sure that blockchain is not empty */
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 2;
        this.miningReward = 100;
        this.pendingTransactions = [];
    }


    createGenesisBlock(): Block {
        return new Block(Date.parse('2000-01-01'), '', []);
    }

    getLatestBlock(): Block {
        return this.chain[this.chain.length - 1]; 
    }

    minePendingTransactions(miningRewardAddress: string): void {
        const rewardTx = new Transaction(miningRewardAddress, this.miningReward, null);
        this.pendingTransactions.push(rewardTx);

        const block = new Block(Date.now(), this.getLatestBlock().hash, this.pendingTransactions);
        block.mineBlock(this.difficulty);

        console.log(`Block succesfully mined`);

        this.chain.push(block);
        this.pendingTransactions = [];
    }

    addTransaction(transaction: Transaction): void {
        if (!transaction.fromAddress || !transaction.toAddress) {
            throw new Error('You must provide from and to address');
        }

        if (!transaction.isValid) {
            throw new Error('Transaction is invalid');
        }

        if (transaction.amount <= 0) {
            throw new Error('Transaction amount must be higher than 0');
        }

        if (this.getBalanceOfAddress(transaction.fromAddress) < transaction.amount) {
            throw new Error('Not enough balance');
        }

        this.pendingTransactions.push(transaction);
        console.log('Transaction added');
    }

    getBalanceOfAddress(address: string): number {
        let balance = 0;
        for (const block of this.chain) {
            for (const trans of block.transactions) {
                if (trans.fromAddress === address) {
                    balance -= trans.amount;
                
                }

                if (trans.toAddress === address) {
                    balance += trans.amount;
                }
            }
        }
        
        return balance;
    }

    isChainValid(): boolean {
        const realGenesis = JSON.stringify(this.createGenesisBlock());

        if (realGenesis !== JSON.stringify(this.chain[0])) {
            return false;
        }

        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (previousBlock.hash !== currentBlock.previousHash) {
                return false;
            }

            if (!currentBlock.hasValidTransactions()) {
                return false;
            }

            if (currentBlock.hash !== calculateHash(currentBlock.previousHash, currentBlock.timestamp, currentBlock.transactions, currentBlock.nonce)) {
                return false;
            }
        }
        return true;
    }
}